﻿## Interface: 30300
## X-Curse-Packaged-Version: 3.3.1
## X-Curse-Project-Name: AceGUI-3.0-SharedMediaWidgets
## X-Curse-Project-ID: ace-gui-3-0-shared-media-widgets
## X-Curse-Repository-ID: wow/ace-gui-3-0-shared-media-widgets/mainline
## X-Curse-Packaged-Version: Beta-100802
## X-Curse-Project-Name: Rebirther
## X-Curse-Project-ID: rebirther
## X-Curse-Repository-ID: wow/rebirther/mainline

## Title: Lib: AceGUI-3.0-SharedMediaWidgets
## Notes: Enables AceGUI-3.0 widgets for the 5 basic SharedMedia-3.0 types
## Version: 3.3.1
## Author: Yssaril
## OptionalDeps: Ace3, LibSharedMedia-3.0
## X-Category: Library

widget.xml